
let CommonUtil = {};

CommonUtil.mockModeParam = "mockMode";
/*Utility Function get Url paramers*/
CommonUtil.getUrlVars = function (url) {
  url = url || window.location.href;
  var vars = {};
  var parts = url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
  });
  console.log("PARTS-"+parts);
  return vars;
}

//export default ApiBase;
export { CommonUtil as default};
