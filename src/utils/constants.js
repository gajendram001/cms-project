let Constants = {};

/**Launching Url Parameters
 * mockMode = true
 * resetMockData = true
 * mockData -> attribute use to store data
 */
Constants.MockMode = { IsMockMode : "mockMode", RestMockData : "resetMockData", MockData: "mockData"};

/**Paging parametes
 * {TotalItems : 0, CurrentPage : 1 , PageSize:0, TotalPages: 1, SortBy :"Name", Descending:false}
 */
Constants.Pagging = { TotalItems:"TotalItems", CurrentPage : "CurrentPage", PageSize : "PageSize", TotalPages:"TotalPages",
SortBy: "SortBy", Descending: "Descending"};

Object.freeze(Constants);
//export default ApiBase;
export { Constants as default};
