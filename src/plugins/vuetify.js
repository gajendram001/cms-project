import Vue from "vue";
import Vuetify from "vuetify/lib";
import colors from "vuetify/lib/util/colors";

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: "mdi",
  },
  theme: {
    themes: {
      light: {
        primary: colors.blue.darken4,
        secondary: colors.grey.darken1,
        accent: colors.shades.black,
        error: colors.red.accent3,
        info: "#2196F3",
        success: "#4CAF50",
        warning: "#FFC107",
        default: "#3c73e9",
        alert:"#e50a0a",
        save: "#3c73e9",
        cancel: "#fff",
        update:"#4caf50",
        edit:colors.shades.black
      },
      dark: {
        primary: colors.blue.lighten4,
        secondary: colors.grey.lighten1,
        accent: colors.shades.light,
        error: colors.red.accent3,
        info: "#2196F3",
        success: "#4CAF50",
        warning: "#FFC107",
        default: "#8f8c8c",
      },
    },
  },
});
