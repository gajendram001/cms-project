let menuMap = {};


menuMap.mainMenu = [
    {title:"Home",icon:"mdi-home-variant",key: "01",id:"cms100"},
    {title:"Customer",icon:"mdi-account-multiple",key: "02",id:"cms200"},
    {title:"Reseller",icon:"mdi-plus-network",key: "03",id:"cms300"},
    {title:"Plans",icon:" mdi-clipboard-text",key: "04",id:"cms400"},
    {title:"App",icon:"mdi-apps",key:"05",id:"cms500"},
    {title:"User",icon:"mdi-account-box",key: "06",id:"cms600"}
],

menuMap.subMenu = [
    { title:"Home",icon:"mdi-home-variant",routeLink:"/home/cms100/chart",key: "011"},
    { title:"Search",icon:"mdi-file-find",routeLink:"/home/cms100/search",key: "012"},
    { title:"Office",icon:" mdi-dropbox",key: "011",subMenus:[
        { title:"Test",icon:" mdi-cube-outline",routeLink:"/home/cms100/test",key: "020"},
    ]},
    { title:"CustomerHome",icon:"mdi-home-variant",routeLink:"/customer/cms200/chart",key: "013"},
    { title:"CustomerSearch",icon:"mdi-file-find",routeLink:"/customer/cms200/search",key: "014"},
    { title:"ResellerHome",icon:"mdi-home-variant",routeLink:"/reseller/cms300/chart",key: "015"},
    { title:"ResellerSearch",icon:"mdi-file-find",routeLink:"/reseller/cms300/search",key: "016"},
    { title:"PlansSearch",icon:"mdi-file-find",routeLink:"/plans/cms400/search",key: "017"},
    { title:"AppSearch",icon:"mdi-file-find",routeLink:"/app/cms500/search",key: "018"},
    { title:"UserSearch",icon:"mdi-file-find",routeLink:"/user/cms600/search",key: "019"},
],

Object.freeze(menuMap);
export { menuMap as default};
