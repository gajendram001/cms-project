//import ApiService from "@/common/api.service";
// import { SIGN_UP} from "../actions.type"
// import { ADD_CUSTOMER } from "../mutations.type"

const registerModule = {
  state: () => ({
    userName: "cloudcodes",
    passWord: "1234",
    authenticate: true,
  }),

  getters: {
    getAuthenticate(state) {
      return state.authenticate;
    },
  },
  mutations: {
    validateUser(state, payLoad) {
      if (state.userName === payLoad.user && state.passWord === payLoad.pass) {
        alert("Login To CloudCodes");
        state.authenticate = true;
      }
      return 0;
    },
    setAuthenticate(state){
        state.authenticate = false;
    }
  },
};

export default registerModule;
