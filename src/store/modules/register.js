//import ApiService from "@/common/api.service";
import { SIGN_UP} from "../actions.type"
import { ADD_CUSTOMER } from "../mutations.type"

const registerModule = {
    state: () => ({
        customers: []

    }),

    mutations: {
        [ADD_CUSTOMER](state, payload) {
            state.customers.push(payload)
        }
    },
    actions: {
       async [SIGN_UP](context, payload){
           console.log("in sign up action", payload)
        //await ApiService.post('customer', payload);
        context.commit(ADD_CUSTOMER, payload)
        //context.dispatch(FETCH_COMMENTS, payload.slug);
       },

    //    [REGISTER_CUSTOMER](payload){
    //        console.log("add verified customer", payload)
    //     // await ApiService.post('customer', payload);
    //    }
    },
}

export default registerModule