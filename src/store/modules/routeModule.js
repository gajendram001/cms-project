//import ApiService from "@/common/api.service";
// import { get } from "@/store/actions.type";
// import { setParentRoute } from "@/store/mutations.type";
// import menuMap from "@/constantsData/menuMap";

const routeModule = {
  state: () => ({
    preParentRouteId: "",
    subMenus: [],
  }),

  mutations: {
    setPreParentID(state, payload) {
      state.preParentRouteId = payload;
    },
    setSubRouteData(state, payload) {
      state.subMenus = [payload.submenus,payload.header,payload.icon];
    }
  },
  getters: {
    getParentRoute(state) {
      return state.subMenus;
    },
  },
  actions: {},
};

export default routeModule;
