import Vue from "vue";
import Vuex from "vuex";
import register from "@/store/modules/register";
import login from "@/store/modules/loginModule";
import routeModule from "@/store/modules/routeModule";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    count: 0,
    auth: false,
  },
  mutations: {
    increment(state) {
      state.count++;
    },
    auth(state, value) {
      state.auth = value;
    },
  },
  actions: {
    increment({ commit }) {
      commit("increment");
    },
  },
  modules: {
    register,
    login,
    routeModule,
  },
});
