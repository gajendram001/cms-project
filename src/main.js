import Vue from "vue";
//import './plugins/axios'
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import i18n from "./i18n";
import store from "./store";
import router from "./router";
import ApiService from "./common/api.service";
import GAuth from 'vue-google-oauth2';

Vue.config.productionTip = false;
Vue.use(GAuth, {
  clientId:
    "768834812579-007e5802er7gj3c93p8qa9568h8bj3na.apps.googleusercontent.com",
  scope: "profile email",
  prompt: "select_account",
  fetch_basic_profile: false,
});
ApiService.init();

new Vue({
  vuetify,
  i18n,
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");
