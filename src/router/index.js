import Vue from "vue";
import VueRouter from "vue-router";
import store from "@/store/index.js";
Vue.use(VueRouter);
const routes = [
  {
    path: "/login",
    name: "loginPage",
    component: () => import("@/components/ccs-login-cms.vue"),
  },
  {
    path: "/pages/home",
    name: "MainLayoutPage",
    component: () => import("@/components/mainLayoutFrame.vue"),
    children: [
      {
        name: "homeChart",
        path: "/home/:id/chart",
        component: () => import("@/components/ccs-home.vue"),
      },
      {
        name: "homeSearch",
        path: "/home/:id/search",
        component: () => import("@/components/ccs-search.vue"),
      },
      {
        name: "homeTest",
        path: "/home/:id/test",
        component: () => import("@/pages/formPage"),
      },
      {
        name: "customerChart",
        path: "/customer/:id/chart",
        component: () => import("@/components/ccs-home.vue"),
      },
      {
        name: "customerSearch",
        path: "/customer/:id/search",
        component: () => import("@/components/ccs-search.vue"),
      },
      {
        name: "resellerChart",
        path: "/reseller/:id/chart",
        component: () => import("@/components/ccs-home.vue"),
      },
      {
        name: "resellerSearch",
        path: "/reseller/:id/search",
        component: () => import("@/components/ccs-search.vue"),
      },
      {
        name: "appSearch",
        path: "/app/:id/search",
        component: () => import("@/components/ccs-search.vue"),
      },
      {
        name: "plansSearch",
        path: "/plans/:id/search",
        component: () => import("@/components/ccs-search.vue"),
      },
      {
        name: "userSearch",
        path: "/user/:id/search",
        component: () => import("@/pages/ccs-user.vue"),
      },
    ],
  },
  {
    path: "*",
    name: "PageNotFound",
    component: () => import("@/pages/pageNotFound.vue"),
    // meta: store.commit("setAuthenticate"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
   if (to.path !== "/login" && !store.getters.getAuthenticate) {
    next({ path: "/login" });
  } else next();
});
export default router;
