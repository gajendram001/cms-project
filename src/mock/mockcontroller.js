import { isArray } from 'util';
import Constants from "../utils/constants";
import CommonUtil from "../utils/commonUtil";

/* Refer this class as static class*/
let MockController = {
    baseURL: "https://reqres.in/api/",
    json: true,
    MockDataObj : {}
}

MockController.execute = function(method, resource, data){
    console.log(method+" , "+resource+" , "+data);
    this.MockDataObj = {};
    if(localStorage.getItem(Constants.MockMode.MockData))
    {
      this.MockDataObj = JSON.parse(localStorage.getItem(Constants.MockMode.MockData));
    }
    else 
    {
      this.MockDataObj = require('./mockdata.json');
      this.Save();
    }
   
    let invoke;
    switch(method.toUpperCase()){
      case "GET":
          invoke = this.Get;
        break;
      case "POST":
          invoke = this.Post;
        break;
       case "PUT":
          invoke = this.Put;
        break;
      case "DELETE":
          invoke = this.Delete;
        break;
    }

    let Context = this;
    return new Promise(function(resolve, reject) {
      console.log("REJECT-"+reject);
        resolve( invoke.call(Context,resource,data));
    });
}

MockController.GetPathArray = function(resource){
  let rawPathArray = resource.split('/');
  let pathArray = [];
  for(let j in rawPathArray){
    if(rawPathArray[j] === "" || rawPathArray[j].indexOf("?") == 0)
      continue;
     pathArray.push(rawPathArray[j])
  }

  return {PathArray : pathArray, 
          Resource : pathArray.join("/"), 
          ParentResource : pathArray.slice(0, pathArray.length -1).join("/"),
          LeafResource :  pathArray[pathArray.length -1] };
}

/**
 * Function to update localstorage
 */
MockController.Save = function(){
  localStorage.setItem(Constants.MockMode.MockData, JSON.stringify(this.MockDataObj));
}


/**
 * function to get data object and object list
 */
MockController.Get = function(resource){
 
  let resourceDetails = this.GetPathArray(resource);
  let pathArray       = resourceDetails.PathArray;
  let currentResource = this.MockDataObj;
  let returnData;
  for(let i=0; i<pathArray.length; i++ ){
    let currentResourcePath = pathArray[i];
    if(isArray(currentResource)){

      for(let j=0; j<currentResource.length; j++){ //Find Data element by id
        if(currentResource[j]["id"] == currentResourcePath){
          currentResource = currentResource[j];
          break;
        }
      }

      //is leaf resource
      if((pathArray.length - 1 == i))
        returnData = {Item : currentResource };

    } else
    {
      currentResource = currentResource[currentResourcePath];

      //is leaf resource
      if((pathArray.length - 1 == i))
      {
        let requestParams = CommonUtil.getUrlVars(resource);
        let pagging = {};
        if(requestParams[Constants.Pagging.PageSize] !== undefined){
          //Prepare Page data
          let PageSize    = parseInt(requestParams[Constants.Pagging.PageSize]);
          let CurrentPage = parseInt(requestParams[Constants.Pagging.CurrentPage]);
          let StartIndex  = (CurrentPage - 1) *PageSize;
          let ItemCount   = (StartIndex + PageSize) < currentResource.length? PageSize : (currentResource.length - StartIndex);
          let Items   = currentResource.slice(StartIndex , StartIndex + ItemCount);
          
          pagging[Constants.Pagging.TotalItems] = currentResource.length;
          pagging[Constants.Pagging.CurrentPage] = CurrentPage;
          pagging[Constants.Pagging.TotalPages] = Math.ceil((currentResource.length / PageSize));
          pagging[Constants.Pagging.PageSize] = PageSize;
          returnData  = {Items :Items };
          returnData.Pages = pagging;
        }else{
          
          pagging[Constants.Pagging.TotalItems]  = currentResource.length;
          pagging[Constants.Pagging.CurrentPage] = 1;
          pagging[Constants.Pagging.TotalPages]  = 1;
          pagging[Constants.Pagging.PageSize]    = currentResource.length;
          returnData = {Items :currentResource };
          returnData.Pages = pagging;
        }
      }
    }
  }
  return returnData;
} 

/**
 * function to delete data object
 */
MockController.Delete = function(resource){
  
  let resourceDetails = this.GetPathArray(resource);
  let ParentList = this.Get(resourceDetails.ParentResource).Items;
  let ToDeleteIndex = -1;
  
  if(ParentList != null && isArray(ParentList)){
    for(let j=0; j<ParentList.length; j++){ //Find Data element by id
      if(ParentList[j]["id"] == resourceDetails.LeafResource){
        ToDeleteIndex = j;
        break;
      }
    }

    if(ToDeleteIndex > -1){
      ParentList.splice(ToDeleteIndex, 1); // Remove Item from arrray
      this.Save();
    }
  }
  return {};
};

/**
 * function to update data object
 */
MockController.Put = function(resource, data){
  // debugger;
  let resourceDetails = this.GetPathArray(resource);
  let ParentList = this.Get(resourceDetails.ParentResource, this.MockDataObj).Items;
  
  if(ParentList != null && isArray(ParentList) && data["id"] == resourceDetails.LeafResource){
    for(let j=0; j<ParentList.length; j++){ //Find Data element by id
      if(ParentList[j]["id"] == resourceDetails.LeafResource){
        ParentList[j] = data; // Object Updated
        break;
      }
    }
    return {Item : data};
  }
};

/**
 * Function to add new data object
 */
MockController.Post = function(resource, data){
  // debugger;
  let resourceDetails = this.GetPathArray(resource);
  let ParentList = this.Get(resourceDetails.Resource).Items;
  
  if(ParentList != null  && isArray(ParentList)){
    if(data["id"] == undefined)
      data["id"] = Date.now();
    ParentList.push(data);
    return {Item : data};
  }
};

//Object.freeze(ApiConfig)
//[ {"id": 1, "email": "george.bluth@reqres.in", "first_name": "George", "last_name": "Bluth"}]
export { MockController as default };