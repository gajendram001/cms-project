let Screens = [
    {
        Module: "Users",
        List: {
            Headers: [
                { text: "FirstName", align: "left", value: "first_name", type: "string", class: "default white--text subtitle-2" },
                { text: "LastName", align: "left", value: "last_name", type: "string", class: "default white--text subtitle-2" },
                { text: "Email", align: "left", value: "email", type: "string", class: "default white--text subtitle-2" },
                { text: "Status", align: "left", value: "isActive", type: "boolean", "hint": " (active/inactive)", class: "default white--text subtitle-2" },
                { text: "Score", align: "left", value: "score", type: "integer", class: "default white--text subtitle-2" },
                { text: "CreatedOn", align: "left", value: "createdOn", type: "date", class: "default white--text subtitle-2" }
            ]
        }
    }
]

export { Screens as default };