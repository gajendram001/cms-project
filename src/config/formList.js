 let FormList = [
    {
        Module: "Users",
        Forms: {
            Id: "Form1",
            DisplayText: "Create User",
            Sections: [
                {
                    
                    Id: "Section1",
                    DisplayText: "User Basic Details",
                    ShowHeader: false,
                    Fields: [
                        { cols: "6", text: "FirstName", align: "left", value: "first_name", type: "string", class: "default white--text subtitle-2" },
                        { text: "LastName", align: "left", value: "last_name", type: "string", class: "default white--text subtitle-2" },
                        { text: "Email", align: "left", value: "email", type: "string", class: "default white--text subtitle-2" },
                        { text: "Status", align: "left", value: "isActive", type: "boolean", "hint": " (active/inactive)", class: "default white--text subtitle-2" },
                        { text: "Score", align: "left", value: "score", type: "integer", class: "default white--text subtitle-2" },
                        { text: "CreatedOn", align: "left", value: "createdOn", type: "date", class: "default white--text subtitle-2" }
                    ]        
                },
                {
                    Id: "Section2",
                    DisplayText: "User Geo Location",
                    ShowHeader: true,
                    Fields: [
                        { cols: "6", text: "FirstName", align: "left", value: "first_name", type: "string", class: "default white--text subtitle-2" },
                        { text: "LastName", align: "left", value: "last_name", type: "string", class: "default white--text subtitle-2" },
                        { text: "Email", align: "left", value: "email", type: "string", class: "default white--text subtitle-2" },
                        { text: "Status", align: "left", value: "isActive", type: "boolean", "hint": " (active/inactive)", class: "default white--text subtitle-2" },
                        { text: "Score", align: "left", value: "score", type: "integer", class: "default white--text subtitle-2" },
                        { text: "CreatedOn", align: "left", value: "createdOn", type: "date", class: "default white--text subtitle-2" }
                    ],
                    Actions: [
                        {
                            
                        }
                    ]        
                }
            ],
            Actions: [
                { text: "FirstName", align: "left", value: "first_name", type: "string", class: "default white--text subtitle-2" },
                { text: "LastName", align: "left", value: "last_name", type: "string", class: "default white--text subtitle-2" },
                { text: "Email", align: "left", value: "email", type: "string", class: "default white--text subtitle-2" },
                { text: "Status", align: "left", value: "isActive", type: "boolean", "hint": " (active/inactive)", class: "default white--text subtitle-2" },
                { text: "Score", align: "left", value: "score", type: "integer", class: "default white--text subtitle-2" },
                { text: "CreatedOn", align: "left", value: "createdOn", type: "date", class: "default white--text subtitle-2" }
            ]
        }
    }
]

export { FormList as default };