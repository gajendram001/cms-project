import axios from "axios";
import ApiConfig from "./apiConfig";
import MockController  from '../mock/mockcontroller';
import CommonUtil from "../utils/commonUtil";
import Constants from "../utils/constants";

const client = axios.create(ApiConfig);

let ApiBase = function  (apipath) {
  this.path = apipath;
  console.log(apipath);
}

/*Function to invoke http call*/
ApiBase.prototype.Execute = async function execute(method, resource, data) {
  // inject the accessToken for each request
  let accessToken = localStorage.getItem("token");
  console.log("HTTP-CALL");
  let url = this.path;
  console.log("PATH-"+this.path);
  console.log("BASE-URL-"+ApiConfig.baseURL);
  if(resource != undefined && !ApiConfig.baseURL.includes('localhost'))
    url += "/" + resource;
  console.log(method+" , "+resource+" , "+data);
  console.log("API-URL"+url);
  return client({
    method,
    url: url,
    data,
    headers: {
      Authorization: `Bearer ${accessToken}`
    }
  })
    .then(res => {
      console.log("RESPONSE-"+res);
      return res;
    })
    .catch(err => {
      return err;
    });
};

let urlParams = CommonUtil.getUrlVars();
if(urlParams[Constants.MockMode.IsMockMode] === "true"){
  /* Clear Local Storage mock data */
  if(urlParams[Constants.MockMode.RestMockData] === "true" && !localStorage.getItem(Constants.MockMode.MockData))
    localStorage.removeItem(Constants.MockMode.MockData)
  
  ApiBase.prototype.Execute =  function execute(method, resource, data) {
    console.log(method+" , "+resource+" , "+data);
    let url = this.path;
    if(resource != undefined)
      url += "/" + resource;

     console.log("GO-FOR-MOCK-CONTROLLER"); 
    return MockController.execute(method, url, data);
  };
}

/*Function to prepare pageing parameters 
  function(String, {TotalItems : 0, CurrentPage : 1 , PageSize:0, TotalPages: 1, SortBy :"Name", Descending:false}) */
ApiBase.prototype.PrepareParams = function(QueryString, Pagination) {
  var parameters = [];

  if (!QueryString) parameters.push(QueryString);

  if(Pagination !== undefined){
    if (Pagination.CurrentPage !== undefined) parameters.push(Constants.Pagging.CurrentPage + "=" + Pagination.CurrentPage);
    if (Pagination.PageSize !== undefined) parameters.push(Constants.Pagging.PageSize + "=" + Pagination.PageSize);
    
    if (Pagination.SortBy !== undefined)  parameters.push(Constants.Pagging.SortBy + "=" + Pagination.SortBy);
    if (Pagination.Descending !== undefined) parameters.push(Constants.Pagging.Descending + "=" + Pagination.Descending);
  }
  return parameters;
};

    
/* Delete API */
ApiBase.prototype.Delete = async function(id){
  return this.Execute("delete", id);
}

/* Get API */
ApiBase.prototype.Get = async function(id){
  return this.Execute("get", id);
}

/* Get All API */
ApiBase.prototype.GetAll = async function(QueryString, Pagination){
  console.log("PAGINATION-"+QueryString);
  return this.Execute("get", `?` + this.PrepareParams(QueryString, Pagination).join("&"));
}

/* Create All API */
ApiBase.prototype.Create = async function(data){
  return this.Execute("post", null, data);
}

/* Create All API */
ApiBase.prototype.Update = async function(id, data){
  return this.Execute("post", id, data);
}
//export default ApiBase;
export { ApiBase as default};
