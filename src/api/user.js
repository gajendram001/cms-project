import ApiBase from "./apiBase";
import { Date } from "core-js";

const UserUrl = "/users";
let User = function  () {
    ApiBase.call(this,UserUrl);
    this.date = Date.now();
}

//Set Parent prototype
User.prototype = Object.create(ApiBase.prototype);
User.prototype.constructor = User;

User.prototype.Print = function(){
    console.log(this.date);
}


export { User as default};