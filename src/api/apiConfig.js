let ApiConfig = {
    // baseURL: "https://reqres.in/api/",
    baseURL: "http://localhost:3000",
    json: true
}

Object.freeze(ApiConfig)

export { ApiConfig as default };