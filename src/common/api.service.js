import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import { API_URL } from "@/common/config";

const ApiService = {
    init() {
        Vue.use(VueAxios, axios);
        Vue.axios.defaults.baseURL = API_URL
    },

    // setHeader() {
    //     Vue.axios.defaults.headers.common[
    //       "Authorization"
    //     ] = `Token ${JwtService.getToken()}`;
    //   },

      query(resource, params) {
        return Vue.axios.get(resource, params).catch(error => {
          throw new Error(`[RWV] ApiService ${error}`);
        });
      },

      get(resource, payload = "") {
        return Vue.axios.get(`${resource}/${payload}`).catch(error => {
          throw new Error(`[RWV] ApiService ${error}`);
        });
      },
      post(resource, params) {
        console.log("resource......", resource, "params.........", params)
        return Vue.axios.post(`${resource}`, params);
      }, 
    
      put(resource, params) {
        return Vue.axios.put(`${resource}`, params);
      },
    
      delete(resource) {
        return Vue.axios.delete(resource).catch(error => {
          throw new Error(`[RWV] ApiService ${error}`);
        });
      }
};
export default ApiService;